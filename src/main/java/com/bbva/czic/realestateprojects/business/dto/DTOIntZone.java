package com.bbva.czic.realestateprojects.business.dto;

import java.io.Serializable;

public class DTOIntZone
        implements Serializable {

    public final static long serialVersionUID = 1L;
    private String id;

    public DTOIntZone() {
        //default constructor
    }

    public DTOIntZone(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
