package com.bbva.czic.realestateprojects.business;

import java.util.List;

import com.bbva.czic.realestateprojects.business.dto.DTOIntRealEstateProjects;

public interface ISrvIntRealEstateProjects {

    public List<DTOIntRealEstateProjects> listRealEstateProjects(String indicador);

    public DTOIntRealEstateProjects getRealEstateProject(String idRealEstateProjects);

    public void createRealEstateProject(DTOIntRealEstateProjects infoRealEstateProjects);

    public void modifyRealEstateProject(String idRealEstateProjects, DTOIntRealEstateProjects infoRealEstateProjects);

}
