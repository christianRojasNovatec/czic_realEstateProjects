package com.bbva.czic.realestateprojects.business.dto;

public class DTOIntLocation {

    private DTOIntState state;
    private DTOIntCity city;
    private DTOIntZone zone;
    private DTOIntSector sector;
    private DTOIntNeighborhood neighborhood;

    /**
     * @return the state
     */
    public DTOIntState getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(DTOIntState state) {
        this.state = state;
    }

    /**
     * @return the city
     */
    public DTOIntCity getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(DTOIntCity city) {
        this.city = city;
    }

    /**
     * @return the zone
     */
    public DTOIntZone getZone() {
        return zone;
    }

    /**
     * @param zone the zone to set
     */
    public void setZone(DTOIntZone zone) {
        this.zone = zone;
    }

    /**
     * @return the sector
     */
    public DTOIntSector getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(DTOIntSector sector) {
        this.sector = sector;
    }

    /**
     * @return the neighborhood
     */
    public DTOIntNeighborhood getNeighborhood() {
        return neighborhood;
    }

    /**
     * @param neighborhood the neighborhood to set
     */
    public void setNeighborhood(DTOIntNeighborhood neighborhood) {
        this.neighborhood = neighborhood;
    }

}
