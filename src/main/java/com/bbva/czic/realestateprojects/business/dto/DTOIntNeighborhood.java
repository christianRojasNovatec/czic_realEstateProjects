package com.bbva.czic.realestateprojects.business.dto;

import java.io.Serializable;

public class DTOIntNeighborhood
        implements Serializable {

    public final static long serialVersionUID = 1L;
    private String id;

    public DTOIntNeighborhood() {
        //default constructor
    }

    public DTOIntNeighborhood(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
