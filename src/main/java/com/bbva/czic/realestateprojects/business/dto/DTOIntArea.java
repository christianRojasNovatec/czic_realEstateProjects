
package com.bbva.czic.realestateprojects.business.dto;




public class DTOIntArea {

    public final static long serialVersionUID = 1L;
    private String id;
    private String size;
    private String measured;

    public DTOIntArea() {
        //default constructor
    }

    public DTOIntArea(String id, String size, String measured) {
        this.id = id;
        this.size = size;
        this.measured = measured;
    }

    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMeasured() {
        return measured;
    }

    public void setMeasured(String measured) {
        this.measured = measured;
    }

}
