package com.bbva.czic.realestateprojects.business.dto;

public class DTOIntPrice {

    public final static long serialVersionUID = 1L;
    private String amount;
    private String currency;

    public DTOIntPrice() {
        //default constructor
    }

    public DTOIntPrice(String amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
