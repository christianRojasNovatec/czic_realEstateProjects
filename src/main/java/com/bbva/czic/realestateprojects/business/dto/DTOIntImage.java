package com.bbva.czic.realestateprojects.business.dto;

public class DTOIntImage {

    public final static long serialVersionUID = 1L;
    private String id;
    private String url;
    private String description;
    private Boolean isActive;
    private Boolean isPreferencial;
    private String order;

    public DTOIntImage() {
        //default constructor
    }

    public DTOIntImage(String id, String url, String description, Boolean isActive, Boolean isPreferencial, String order) {
        this.id = id;
        this.url = url;
        this.description = description;
        this.isActive = isActive;
        this.isPreferencial = isPreferencial;
        this.order = order;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsPreferencial() {
        return isPreferencial;
    }

    public void setIsPreferencial(Boolean isPreferencial) {
        this.isPreferencial = isPreferencial;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

}
