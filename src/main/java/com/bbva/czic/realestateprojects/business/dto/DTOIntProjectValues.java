package com.bbva.czic.realestateprojects.business.dto;

public class DTOIntProjectValues {

    public final static long serialVersionUID = 1L;
    private String id;
    private DTOIntPrice price;

    public DTOIntProjectValues() {
        //default constructor
    }

    public DTOIntProjectValues(String id, String amount, String currency) {
        this.id = id;
        this.price = new DTOIntPrice(amount, currency);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DTOIntPrice getPrice() {
        return price;
    }

    public void setPrice(DTOIntPrice price) {
        this.price = price;
    }

}
