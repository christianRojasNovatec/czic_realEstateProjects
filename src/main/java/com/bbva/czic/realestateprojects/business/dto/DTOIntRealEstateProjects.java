package com.bbva.czic.realestateprojects.business.dto;

import java.util.List;

public class DTOIntRealEstateProjects {

    public final static long serialVersionUID = 1L;
    private String id;
    private String name;
    private String stratum;
    private String numberBathrooms;
    private String isDecorated;
    private String characteristics;
    private String descriptionRealEstateProject;
    private List<DTOIntArea> areas;
    private List<DTOIntProjectValues> projectPrice;
    private List<DTOIntFeature> features;
    private DTOIntLocation location;
    private String availableUnits;
    private List<DTOIntImage> images;

    public DTOIntRealEstateProjects() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStratum() {
        return stratum;
    }

    public void setStratum(String stratum) {
        this.stratum = stratum;
    }

    public String getNumberBathrooms() {
        return numberBathrooms;
    }

    public void setNumberBathrooms(String numberBathrooms) {
        this.numberBathrooms = numberBathrooms;
    }

    public String getIsDecorated() {
        return isDecorated;
    }

    public void setIsDecorated(String isDecorated) {
        this.isDecorated = isDecorated;
    }

    public String getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(String characteristics) {
        this.characteristics = characteristics;
    }

    public String getDescriptionRealEstateProject() {
        return descriptionRealEstateProject;
    }

    public void setDescriptionRealEstateProject(String descriptionRealEstateProject) {
        this.descriptionRealEstateProject = descriptionRealEstateProject;
    }

    public List<DTOIntArea> getAreas() {
        return areas;
    }

    public void setAreas(List<DTOIntArea> areas) {
        this.areas = areas;
    }

    public List<DTOIntProjectValues> getProjectPrice() {
        return projectPrice;
    }

    public void setProjectPrice(List<DTOIntProjectValues> projectPrice) {
        this.projectPrice = projectPrice;
    }

    public List<DTOIntFeature> getFeatures() {
        return features;
    }

    public void setFeatures(List<DTOIntFeature> features) {
        this.features = features;
    }

    public DTOIntLocation getLocation() {
        return location;
    }

    public void setLocation(DTOIntLocation location) {
        this.location = location;
    }

    public String getAvailableUnits() {
        return availableUnits;
    }

    public void setAvailableUnits(String availableUnits) {
        this.availableUnits = availableUnits;
    }

    public List<DTOIntImage> getImages() {
        return images;
    }

    public void setImages(List<DTOIntImage> images) {
        this.images = images;
    }

}
