package com.bbva.czic.realestateprojects.business.dto;

public class DTOIntFeature {

    public final static long serialVersionUID = 1L;
    private String id;
    private String featuresType;
    private String name;
    private String amount;
    private String featuresDescription;

    public DTOIntFeature() {
        //default constructor
    }

    public DTOIntFeature(String id, String featuresType, String name, String amount, String featuresDescription) {
        this.id = id;
        this.featuresType = featuresType;
        this.name = name;
        this.amount = amount;
        this.featuresDescription = featuresDescription;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFeaturesType() {
        return featuresType;
    }

    public void setFeaturesType(String featuresType) {
        this.featuresType = featuresType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getFeaturesDescription() {
        return featuresDescription;
    }

    public void setFeaturesDescription(String featuresDescription) {
        this.featuresDescription = featuresDescription;
    }

}
