package com.bbva.czic.realestateprojects.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;

import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;

import com.bbva.czic.realestateprojects.business.dto.DTOIntRealEstateProjects;
import com.bbva.czic.realestateprojects.business.ISrvIntRealEstateProjects;
import com.bbva.czic.realestateprojects.business.dto.DTOIntArea;
import com.bbva.czic.realestateprojects.business.dto.DTOIntCity;
import com.bbva.czic.realestateprojects.business.dto.DTOIntFeature;
import com.bbva.czic.realestateprojects.business.dto.DTOIntImage;
import com.bbva.czic.realestateprojects.business.dto.DTOIntLocation;
import com.bbva.czic.realestateprojects.business.dto.DTOIntNeighborhood;
import com.bbva.czic.realestateprojects.business.dto.DTOIntProjectValues;
import com.bbva.czic.realestateprojects.business.dto.DTOIntSector;
import com.bbva.czic.realestateprojects.business.dto.DTOIntZone;
import com.bbva.czic.realestateprojects.dao.RealEstateProjectsDAO;
import com.bbva.czic.realestateprojects.dao.model.ugaj.FormatoUGAJCE01;
import com.bbva.czic.realestateprojects.dao.model.ugaj.FormatoUGAJFS01;
import com.bbva.czic.realestateprojects.dao.model.ugak.FormatoUGAKFE01;
import com.bbva.czic.realestateprojects.dao.model.ugal.FormatoUGALFE00;
import com.bbva.czic.realestateprojects.dao.model.ugal.FormatoUGALFS01;
import com.bbva.czic.realestateprojects.dao.model.ugao.FormatoUGAOFE01;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import java.util.ArrayList;
import javax.annotation.Resource;

@Service
public class SrvIntRealEstateProjects implements ISrvIntRealEstateProjects {

    private static I18nLog log = I18nLogFactory.getLogI18n(SrvIntRealEstateProjects.class, "META-INF/spring/i18n/log/mensajesLog");

    @Autowired
    BusinessServicesToolKit bussinesToolKit;

    @Resource(name = "realEstateProjects-dao")
    private RealEstateProjectsDAO realEstateProjectsDAO;

    @Override
    public List<DTOIntRealEstateProjects> listRealEstateProjects(String indicador) {
        FormatoUGAJCE01 formatoUGAJCE01 = new FormatoUGAJCE01();
        formatoUGAJCE01.setIndlis(indicador);
        List<CopySalida> copySalidas = realEstateProjectsDAO.listRealEstateProjects(formatoUGAJCE01);
        List<DTOIntRealEstateProjects> realEstateProjectses = new ArrayList<DTOIntRealEstateProjects>();
        for (CopySalida copySalida : copySalidas) {
            realEstateProjectses.add(cargarrealEstateProjectDto(copySalida.getCopy(FormatoUGAJFS01.class)));
        }
        return realEstateProjectses;
    }

    @Override
    public DTOIntRealEstateProjects getRealEstateProject(String idRealEstateProjects) {
        FormatoUGALFE00 formatoUGALFE01 = new FormatoUGALFE00();
        formatoUGALFE01.setCodproy(Integer.parseInt(idRealEstateProjects));
        FormatoUGALFS01 formatoUGALFS01 = realEstateProjectsDAO.getRealEstateProject(formatoUGALFE01);
        if (formatoUGALFS01 == null) {
            BusinessServiceException bse = new BusinessServiceException("");
            bse.setErrorMessage("No se encontro información");
            throw bse;
        }
        return cargarFormatoUGALFS01(formatoUGALFS01);
    }

    @Override
    public void createRealEstateProject(DTOIntRealEstateProjects infoRealEstateProjects) {
        realEstateProjectsDAO.createRealEstateProject(cargarFormatoUGAKFE01(infoRealEstateProjects));
    }

    @Override
    public void modifyRealEstateProject(String idRealEstateProjects, DTOIntRealEstateProjects infoRealEstateProjects) {
        realEstateProjectsDAO.modifyRealEstateProject(cargarFormatoUGAOFE01(idRealEstateProjects, infoRealEstateProjects));
    }

    private FormatoUGAKFE01 cargarFormatoUGAKFE01(DTOIntRealEstateProjects infoRealEstateProjects) {
        FormatoUGAKFE01 formatoUGAKFE01 = new FormatoUGAKFE01();
        formatoUGAKFE01.setCodproy(Integer.parseInt(infoRealEstateProjects.getId()));
        for (DTOIntProjectValues dTOIntProjectValues : infoRealEstateProjects.getProjectPrice()) {
            if (dTOIntProjectValues.getId().equals("STARTING_FROM")) {
                formatoUGAKFE01.setVlrdesd(Long.parseLong(dTOIntProjectValues.getPrice().getAmount()));
            }
            if (dTOIntProjectValues.getId().equals("VALOR_ADMIN")) {
                formatoUGAKFE01.setVlradmi(Integer.parseInt(dTOIntProjectValues.getPrice().getAmount()));
            }
        }
        for (DTOIntArea dTOIntProjectValues : infoRealEstateProjects.getAreas()) {
            if (dTOIntProjectValues.getId().equals("BUILT_AREA")) {
                formatoUGAKFE01.setAread(Integer.parseInt(dTOIntProjectValues.getSize()));
            }
        }
        formatoUGAKFE01.setCodciud(Integer.parseInt(infoRealEstateProjects.getLocation().getCity().getId()));
        formatoUGAKFE01.setCodzona(Integer.parseInt(infoRealEstateProjects.getLocation().getZone().getId()));
        formatoUGAKFE01.setSector(infoRealEstateProjects.getLocation().getSector().getId());
        formatoUGAKFE01.setBarrio(infoRealEstateProjects.getLocation().getNeighborhood().getId());
        formatoUGAKFE01.setEstrato(Integer.parseInt(infoRealEstateProjects.getStratum()));
        formatoUGAKFE01.setIndesta(infoRealEstateProjects.getIsDecorated());//TODO: AJustar no se sabe
        formatoUGAKFE01.setNuminmb(Integer.parseInt(infoRealEstateProjects.getAvailableUnits()));//TODO: AJustar no se sabe

        return formatoUGAKFE01;
    }

    private DTOIntRealEstateProjects cargarrealEstateProjectDto(FormatoUGAJFS01 formatoUGAJFS01) {
        DTOIntRealEstateProjects realEstateProjects = new DTOIntRealEstateProjects();
        realEstateProjects.setId("" + formatoUGAJFS01.getCodpro());
        realEstateProjects.setName(formatoUGAJFS01.getNompror());
        DTOIntLocation location = new DTOIntLocation();
        location.setCity(new DTOIntCity("" + formatoUGAJFS01.getCiudad()));
        realEstateProjects.setLocation(location);

        realEstateProjects.setFeatures(new ArrayList<DTOIntFeature>());
        realEstateProjects.getFeatures().add(new DTOIntFeature("ESTADO_PROYECTO", "", "", "", formatoUGAJFS01.getEstado()));
        realEstateProjects.getFeatures().add(new DTOIntFeature("INDICA_IMAGEN", "", "", "", formatoUGAJFS01.getSimagen()));
        realEstateProjects.getFeatures().add(new DTOIntFeature("INDICA_IMUEBLE", "", "", "", formatoUGAJFS01.getSinmueb()));

        return realEstateProjects;
    }

    private DTOIntRealEstateProjects cargarFormatoUGALFS01(FormatoUGALFS01 formatoUGALFS01) {
        DTOIntRealEstateProjects realEstateProjects = new DTOIntRealEstateProjects();
        realEstateProjects.setId("" + formatoUGALFS01.getCodproy());
        realEstateProjects.setAreas(new ArrayList<DTOIntArea>());
        realEstateProjects.getAreas().add(new DTOIntArea("AREA_DESDE", "" + formatoUGALFS01.getAread(), "m"));
        realEstateProjects.getAreas().add(new DTOIntArea("AREA_PRIVADA", "" + formatoUGALFS01.getAreapri(), "m"));
        realEstateProjects.getAreas().add(new DTOIntArea("AREA_CONSTRUIDA", "" + formatoUGALFS01.getAreacon(), "m"));
        realEstateProjects.setProjectPrice(new ArrayList<DTOIntProjectValues>());
        realEstateProjects.getProjectPrice().add(new DTOIntProjectValues("VALOR_MINIMO_PROYECTO", "" + formatoUGALFS01.getVlrdesd(), "COP"));
        realEstateProjects.getProjectPrice().add(new DTOIntProjectValues("VALOR_ADMIN", "" + formatoUGALFS01.getVlradmi(), "COP"));
        realEstateProjects.getProjectPrice().add(new DTOIntProjectValues("VALOR_INMUEBLE", "" + formatoUGALFS01.getVlrinmb(), "COP"));

        DTOIntLocation location = new DTOIntLocation();
        location.setCity(new DTOIntCity("" + formatoUGALFS01.getCodciud()));
        location.setZone(new DTOIntZone("" + formatoUGALFS01.getCodzona()));
        location.setSector(new DTOIntSector("" + formatoUGALFS01.getSector()));
        location.setNeighborhood(new DTOIntNeighborhood("" + formatoUGALFS01.getBarrio()));
        realEstateProjects.setLocation(location);
        realEstateProjects.setStratum("" + formatoUGALFS01.getEstrato());
        realEstateProjects.setIsDecorated(formatoUGALFS01.getIndacab());
        realEstateProjects.setNumberBathrooms("" + formatoUGALFS01.getNbanios());
        realEstateProjects.setFeatures(new ArrayList<DTOIntFeature>());
        realEstateProjects.getFeatures().add(new DTOIntFeature("CARACTERISTICA_UNO", "", "", "", formatoUGALFS01.getCaract1()));
        realEstateProjects.getFeatures().add(new DTOIntFeature("CARACTERISTICA_DOS", "", "", "", formatoUGALFS01.getCaract2()));
        realEstateProjects.getFeatures().add(new DTOIntFeature("NUMERO_HABITACIONES", "", "", "", "" + formatoUGALFS01.getNuminmb()));
        realEstateProjects.getFeatures().add(new DTOIntFeature("ESTADO_PROYECTO", "", "", "", formatoUGALFS01.getIndesta()));
        realEstateProjects.getFeatures().add(new DTOIntFeature("TIPO_INMUEBLE", "", "", "", formatoUGALFS01.getTipinmu()));
        realEstateProjects.setImages(new ArrayList<DTOIntImage>());
        realEstateProjects.getImages().add(new DTOIntImage("" + formatoUGALFS01.getCodimag(),
                formatoUGALFS01.getUrlimag(),
                formatoUGALFS01.getNomimag(),
                true, true, "" + formatoUGALFS01.getOrdenim()));

        return realEstateProjects;
    }

    private FormatoUGAOFE01 cargarFormatoUGAOFE01(String idRealEstateProjects, DTOIntRealEstateProjects infoRealEstateProjects) {
        FormatoUGAOFE01 formatoUGAKFE01 = new FormatoUGAOFE01();
        formatoUGAKFE01.setCodproy(Integer.parseInt(idRealEstateProjects));
        if (infoRealEstateProjects.getProjectPrice() != null) {
            for (DTOIntProjectValues dTOIntProjectValues : infoRealEstateProjects.getProjectPrice()) {
                if (dTOIntProjectValues.getId().equals("VALOR_MINIMO_PROYECTO")) {
                    formatoUGAKFE01.setVlrdesd(Long.parseLong(dTOIntProjectValues.getPrice().getAmount()));
                }
                if (dTOIntProjectValues.getId().equals("VALOR_ADMIN")) {
                    formatoUGAKFE01.setVlradmi(Integer.parseInt(dTOIntProjectValues.getPrice().getAmount()));
                }
                if (dTOIntProjectValues.getId().equals("VALOR_INMUEBLE")) {
                    formatoUGAKFE01.setVlrinmb(Long.parseLong(dTOIntProjectValues.getPrice().getAmount()));
                }
            }
        }
        if (infoRealEstateProjects.getAreas() != null) {
            for (DTOIntArea dTOIntProjectValues : infoRealEstateProjects.getAreas()) {
                if (dTOIntProjectValues.getId().equals("BUILT_AREA")) {
                    formatoUGAKFE01.setAread(Integer.parseInt(dTOIntProjectValues.getSize()));
                }
                if (dTOIntProjectValues.getId().equals("AREA_PRIVADA")) {
                    formatoUGAKFE01.setAreapri(Integer.parseInt(dTOIntProjectValues.getSize()));
                }
                if (dTOIntProjectValues.getId().equals("AREA_CONSTRUIDA")) {
                    formatoUGAKFE01.setAreacon(Integer.parseInt(dTOIntProjectValues.getSize()));
                }
            }
        }
        formatoUGAKFE01.setCodciud(Integer.parseInt(infoRealEstateProjects.getLocation().getCity().getId()));
        formatoUGAKFE01.setCodzona(Integer.parseInt(infoRealEstateProjects.getLocation().getZone().getId()));
        formatoUGAKFE01.setSector(infoRealEstateProjects.getLocation().getSector().getId());
        formatoUGAKFE01.setBarrio(infoRealEstateProjects.getLocation().getNeighborhood().getId());
        formatoUGAKFE01.setEstrato(Integer.parseInt(infoRealEstateProjects.getStratum()));
        formatoUGAKFE01.setIndesta(infoRealEstateProjects.getIsDecorated());//TODO: AJustar no se sabe

        if (infoRealEstateProjects.getFeatures() != null) {
            for (DTOIntFeature feature : infoRealEstateProjects.getFeatures()) {
                if (feature.getId().equals("NUMERO_INMUEBLE")) {
                    formatoUGAKFE01.setNuminmb(Integer.parseInt(feature.getAmount()));
                }
                if (feature.getId().equals("NUMERO_HABITACIONES")) {
                    formatoUGAKFE01.setNhabita(Integer.parseInt(feature.getAmount()));
                }
                if (feature.getId().equals("CARACTERISTICA_UNO")) {
                    formatoUGAKFE01.setCaract1(feature.getAmount());
                }
                if (feature.getId().equals("CARACTERISTICA_DOS")) {
                    formatoUGAKFE01.setCaract2(feature.getAmount());
                }
            }
        }
        formatoUGAKFE01.setNbanios(Integer.parseInt(infoRealEstateProjects.getNumberBathrooms()));
        formatoUGAKFE01.setIndacab(infoRealEstateProjects.getIsDecorated());

        if (infoRealEstateProjects.getImages() != null) {
            for (DTOIntImage feature : infoRealEstateProjects.getImages()) {
                formatoUGAKFE01.setCodimag(Integer.parseInt(feature.getId()));
                formatoUGAKFE01.setNomimag(feature.getDescription());
                formatoUGAKFE01.setUrlimag(feature.getUrl());
                formatoUGAKFE01.setOrdenim(Integer.parseInt(feature.getOrder()));
            }
        }

        return formatoUGAKFE01;
    }

}
