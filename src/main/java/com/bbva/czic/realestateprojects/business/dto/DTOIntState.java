package com.bbva.czic.realestateprojects.business.dto;

import java.io.Serializable;

public class DTOIntState
        implements Serializable {

    public final static long serialVersionUID = 1L;

    private String id;

    public DTOIntState() {
        //default constructor
    }

    public DTOIntState(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
