package com.bbva.czic.realestateprojects.business.dto;

import java.io.Serializable;

public class DTOIntSector
        implements Serializable {

    public final static long serialVersionUID = 1L;
    private String id;

    public DTOIntSector() {
        //default constructor
    }

    public DTOIntSector(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
