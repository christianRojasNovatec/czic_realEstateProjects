
package com.bbva.czic.realestateprojects.facade.v00.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "projectValues", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlType(name = "projectValues", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProjectValues
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Unique Identifier of Real Estate Projects Values", required = true)
    private String id;
    @ApiModelProperty(value = "Value Area.", required = true)
    private ProjectPrice price;

    public ProjectValues() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProjectPrice getPrice() {
        return price;
    }

    public void setPrice(ProjectPrice price) {
        this.price = price;
    }

}
