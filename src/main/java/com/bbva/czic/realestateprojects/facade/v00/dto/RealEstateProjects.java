
package com.bbva.czic.realestateprojects.facade.v00.dto;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "realEstateProjects", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlType(name = "realEstateProjects", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class RealEstateProjects
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Unique Identifier Real Estate Projects", required = true)
    private String id;
    @ApiModelProperty(value = "Name of the real estate project", required = true)
    private String name;
    @ApiModelProperty(value = "Stratum of the real estate project", required = true)
    private String stratum;
    @ApiModelProperty(value = "Number of bathrooms", required = true)
    private String numberBathrooms;
    @ApiModelProperty(value = "House Decorated", required = true)
    private String isDecorated;
    @ApiModelProperty(value = "House Characteristics", required = true)
    private String characteristics;
    @ApiModelProperty(value = "Description of the real estate project", required = true)
    private String descriptionRealEstateProject;
    @ApiModelProperty(value = "Areas del inmueble", required = true)
    private List<Area> areas;
    @ApiModelProperty(value = "Precios del proyecto", required = true)
    private List<ProjectPrice> projectPrice;
    @ApiModelProperty(value = "Caracteristicas del proyecto", required = true)
    private List<Feature> features;
    @ApiModelProperty(value = "Locacion del proyecto", required = true)
    private Location location;
    @ApiModelProperty(value = "Available Units", required = true)
    private String availableUnits;
    @ApiModelProperty(value = "Listado de imagenes del proyecto", required = true)
    private List<Image> images;

    public RealEstateProjects() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStratum() {
        return stratum;
    }

    public void setStratum(String stratum) {
        this.stratum = stratum;
    }

    public String getNumberBathrooms() {
        return numberBathrooms;
    }

    public void setNumberBathrooms(String numberBathrooms) {
        this.numberBathrooms = numberBathrooms;
    }

    public String getIsDecorated() {
        return isDecorated;
    }

    public void setIsDecorated(String isDecorated) {
        this.isDecorated = isDecorated;
    }

    public String getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(String characteristics) {
        this.characteristics = characteristics;
    }

    public String getDescriptionRealEstateProject() {
        return descriptionRealEstateProject;
    }

    public void setDescriptionRealEstateProject(String descriptionRealEstateProject) {
        this.descriptionRealEstateProject = descriptionRealEstateProject;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public List<ProjectPrice> getProjectPrice() {
        return projectPrice;
    }

    public void setProjectPrice(List<ProjectPrice> projectPrice) {
        this.projectPrice = projectPrice;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getAvailableUnits() {
        return availableUnits;
    }

    public void setAvailableUnits(String availableUnits) {
        this.availableUnits = availableUnits;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

}
