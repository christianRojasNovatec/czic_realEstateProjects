package com.bbva.czic.realestateprojects.facade.v00.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "area", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlType(name = "area", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Area
        implements Serializable {

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Unique Identifier Real Estate Projects", required = true)
    private String id;
    @ApiModelProperty(value = "Value Area.", required = true)
    private String size;
    @ApiModelProperty(value = "Measured.", required = true)
    private String measured;

    public Area() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMeasured() {
        return measured;
    }

    public void setMeasured(String measured) {
        this.measured = measured;
    }

}
