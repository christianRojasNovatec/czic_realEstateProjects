package com.bbva.czic.realestateprojects.facade.v00.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "projectPrice", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlType(name = "projectPrice", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProjectPrice
        implements Serializable {

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Id.", required = true)
    private String id;
    @ApiModelProperty(value = "price", required = true)
    private Price price;

    public ProjectPrice() {
        //default constructor
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the price
     */
    public Price getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Price price) {
        this.price = price;
    }

}
