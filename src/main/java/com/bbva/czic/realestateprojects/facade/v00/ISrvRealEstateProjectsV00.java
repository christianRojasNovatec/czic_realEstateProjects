package com.bbva.czic.realestateprojects.facade.v00;

import java.util.List;
import javax.ws.rs.core.Response;

import com.bbva.czic.realestateprojects.facade.v00.dto.RealEstateProjects;
import com.bbva.czic.realestateprojects.facade.v00.dto.ProjectValues;
import com.bbva.czic.realestateprojects.facade.v00.dto.Area;
import com.bbva.czic.realestateprojects.facade.v00.dto.ProjectPrice;
import com.bbva.czic.realestateprojects.facade.v00.dto.Status;
import com.bbva.czic.realestateprojects.facade.v00.dto.Location;
import com.bbva.czic.realestateprojects.facade.v00.dto.Image;
import com.bbva.czic.realestateprojects.facade.v00.dto.Feature;

public interface ISrvRealEstateProjectsV00 {

    public List<RealEstateProjects> listRealEstateProjects(String filter, String fields, String expands, String orderby);

    public RealEstateProjects getRealEstateProject(String idRealEstateProjects);

    public Response createRealEstateProject(RealEstateProjects infoRealEstateProjects);

    public Response modifyRealEstateProject(String idRealEstateProjects, RealEstateProjects infoRealEstateProjects);

}
