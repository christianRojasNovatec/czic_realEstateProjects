package com.bbva.czic.realestateprojects.facade.v00.mapper;

import org.springframework.beans.BeanUtils;

import com.bbva.czic.realestateprojects.facade.v00.dto.RealEstateProjects;
import com.bbva.czic.realestateprojects.facade.v00.dto.ProjectValues;
import com.bbva.czic.realestateprojects.facade.v00.dto.Area;
import com.bbva.czic.realestateprojects.facade.v00.dto.ProjectPrice;
import com.bbva.czic.realestateprojects.facade.v00.dto.Status;
import com.bbva.czic.realestateprojects.facade.v00.dto.Location;
import com.bbva.czic.realestateprojects.facade.v00.dto.Image;
import com.bbva.czic.realestateprojects.facade.v00.dto.Feature;
import com.bbva.czic.realestateprojects.business.dto.DTOIntRealEstateProjects;
import com.bbva.czic.realestateprojects.business.dto.DTOIntProjectValues;
import com.bbva.czic.realestateprojects.business.dto.DTOIntArea;
import com.bbva.czic.realestateprojects.business.dto.DTOIntCity;
import com.bbva.czic.realestateprojects.business.dto.DTOIntPrice;
import com.bbva.czic.realestateprojects.business.dto.DTOIntStatus;
import com.bbva.czic.realestateprojects.business.dto.DTOIntLocation;
import com.bbva.czic.realestateprojects.business.dto.DTOIntImage;
import com.bbva.czic.realestateprojects.business.dto.DTOIntFeature;
import com.bbva.czic.realestateprojects.business.dto.DTOIntNeighborhood;
import com.bbva.czic.realestateprojects.business.dto.DTOIntSector;
import com.bbva.czic.realestateprojects.business.dto.DTOIntState;
import com.bbva.czic.realestateprojects.business.dto.DTOIntZone;
import com.bbva.czic.realestateprojects.facade.v00.dto.City;
import com.bbva.czic.realestateprojects.facade.v00.dto.Neighborhood;
import com.bbva.czic.realestateprojects.facade.v00.dto.Price;
import com.bbva.czic.realestateprojects.facade.v00.dto.Sector;
import com.bbva.czic.realestateprojects.facade.v00.dto.State;
import com.bbva.czic.realestateprojects.facade.v00.dto.Zone;
import java.util.ArrayList;

public class Mapper {

    public static RealEstateProjects realEstateProjectsMap(DTOIntRealEstateProjects intRealEstateProjects) {
        RealEstateProjects realEstateProjects = new RealEstateProjects();
        if (intRealEstateProjects.getAreas() != null) {
            realEstateProjects.setAreas(new ArrayList<Area>());
            for (DTOIntArea area : intRealEstateProjects.getAreas()) {
                realEstateProjects.getAreas().add(areaMap(area));
            }
        }
        realEstateProjects.setAvailableUnits(intRealEstateProjects.getAvailableUnits());
        realEstateProjects.setCharacteristics(intRealEstateProjects.getCharacteristics());
        realEstateProjects.setDescriptionRealEstateProject(intRealEstateProjects.getDescriptionRealEstateProject());
        if (intRealEstateProjects.getFeatures() != null) {
            realEstateProjects.setFeatures(new ArrayList<Feature>());
            for (DTOIntFeature feature : intRealEstateProjects.getFeatures()) {
                realEstateProjects.getFeatures().add(featureMap(feature));
            }
        }
        intRealEstateProjects.setId(realEstateProjects.getId());
        intRealEstateProjects.setIsDecorated(realEstateProjects.getIsDecorated());
        if (intRealEstateProjects.getImages() != null) {
            realEstateProjects.setImages(new ArrayList<Image>());
            for (DTOIntImage image : intRealEstateProjects.getImages()) {
                realEstateProjects.getImages().add(imageMap(image));
            }
        }
        realEstateProjects.setLocation(locationMap(intRealEstateProjects.getLocation()));
        realEstateProjects.setName(intRealEstateProjects.getName());
        realEstateProjects.setNumberBathrooms(intRealEstateProjects.getNumberBathrooms());
        realEstateProjects.setStratum(intRealEstateProjects.getStratum());

        if (intRealEstateProjects.getProjectPrice() != null) {
            realEstateProjects.setProjectPrice(new ArrayList<ProjectPrice>());
            for (DTOIntProjectValues projectValues : intRealEstateProjects.getProjectPrice()) {
                ProjectPrice projectPrice = new ProjectPrice();
                projectPrice.setId(projectValues.getId());
                if (projectPrice.getPrice() != null) {
                    Price price = new Price();
                    price.setAmount(projectValues.getPrice().getAmount());
                    price.setCurrency(projectValues.getPrice().getCurrency());
                    projectPrice.setPrice(price);
                }
                realEstateProjects.getProjectPrice().add(projectPrice);
            }
        }
        return realEstateProjects;
    }

    public static DTOIntRealEstateProjects dtoIntRealEstateProjectsMap(RealEstateProjects realEstateProjects) {
        DTOIntRealEstateProjects dtoIntRealEstateProjects = new DTOIntRealEstateProjects();
        if (realEstateProjects.getAreas() != null) {
            dtoIntRealEstateProjects.setAreas(new ArrayList<DTOIntArea>());
            for (Area area : realEstateProjects.getAreas()) {
                dtoIntRealEstateProjects.getAreas().add(dtoIntAreaMap(area));
            }
        }
        dtoIntRealEstateProjects.setAvailableUnits(realEstateProjects.getAvailableUnits());
        dtoIntRealEstateProjects.setCharacteristics(realEstateProjects.getCharacteristics());
        dtoIntRealEstateProjects.setDescriptionRealEstateProject(realEstateProjects.getDescriptionRealEstateProject());
        if (realEstateProjects.getFeatures() != null) {
            dtoIntRealEstateProjects.setFeatures(new ArrayList<DTOIntFeature>());
            for (Feature feature : realEstateProjects.getFeatures()) {
                dtoIntRealEstateProjects.getFeatures().add(dtoIntFeatureMap(feature));
            }
        }
        dtoIntRealEstateProjects.setId(realEstateProjects.getId());
        dtoIntRealEstateProjects.setIsDecorated(realEstateProjects.getIsDecorated());
        if (realEstateProjects.getImages() != null) {
            dtoIntRealEstateProjects.setImages(new ArrayList<DTOIntImage>());
            for (Image image : realEstateProjects.getImages()) {
                dtoIntRealEstateProjects.getImages().add(dtoIntImageMap(image));
            }
        }
        dtoIntRealEstateProjects.setLocation(dtoIntLocationMap(realEstateProjects.getLocation()));
        dtoIntRealEstateProjects.setName(realEstateProjects.getName());
        dtoIntRealEstateProjects.setNumberBathrooms(realEstateProjects.getNumberBathrooms());
        dtoIntRealEstateProjects.setStratum(realEstateProjects.getStratum());

        if (realEstateProjects.getProjectPrice() != null) {
            dtoIntRealEstateProjects.setProjectPrice(new ArrayList<DTOIntProjectValues>());
            for (ProjectPrice projectPrice : realEstateProjects.getProjectPrice()) {
                DTOIntProjectValues projectValues = new DTOIntProjectValues();
                projectValues.setId(projectPrice.getId());
                if (projectPrice.getPrice() != null) {
                    DTOIntPrice dTOIntPrice = new DTOIntPrice();
                    dTOIntPrice.setAmount(projectPrice.getPrice().getAmount());
                    dTOIntPrice.setCurrency(projectPrice.getPrice().getCurrency());
                    projectValues.setPrice(dTOIntPrice);
                }
                dtoIntRealEstateProjects.getProjectPrice().add(projectValues);
            }
        }
        return dtoIntRealEstateProjects;
    }

    public static ProjectValues projectValuesMap(DTOIntProjectValues dtoIntProjectValues) {
        ProjectValues projectValues = new ProjectValues();
        BeanUtils.copyProperties(dtoIntProjectValues, projectValues);
        return projectValues;
    }

    public static DTOIntProjectValues dtoIntProjectValuesMap(ProjectValues projectValues) {
        DTOIntProjectValues dtoIntProjectValues = new DTOIntProjectValues();
        BeanUtils.copyProperties(projectValues, dtoIntProjectValues);
        return dtoIntProjectValues;
    }

    public static Area areaMap(DTOIntArea dtoIntArea) {
        Area area = new Area();
        BeanUtils.copyProperties(dtoIntArea, area);
        return area;
    }

    public static DTOIntArea dtoIntAreaMap(Area area) {
        DTOIntArea dtoIntArea = new DTOIntArea();
        BeanUtils.copyProperties(area, dtoIntArea);
        return dtoIntArea;
    }

    public static ProjectPrice priceMap(DTOIntPrice dtoIntPrice) {
        ProjectPrice price = new ProjectPrice();
        BeanUtils.copyProperties(dtoIntPrice, price);
        return price;
    }

    public static DTOIntPrice dtoIntPriceMap(ProjectPrice price) {
        DTOIntPrice dtoIntPrice = new DTOIntPrice();
        BeanUtils.copyProperties(price, dtoIntPrice);
        return dtoIntPrice;
    }

    public static Status statusMap(DTOIntStatus dtoIntStatus) {
        Status status = new Status();
        BeanUtils.copyProperties(dtoIntStatus, status);
        return status;
    }

    public static DTOIntStatus dtoIntStatusMap(Status status) {
        DTOIntStatus dtoIntStatus = new DTOIntStatus();
        BeanUtils.copyProperties(status, dtoIntStatus);
        return dtoIntStatus;
    }

    public static Location locationMap(DTOIntLocation dtoIntLocation1) {
       Location location = new Location();
        if (dtoIntLocation1.getCity() != null) {
            location.setCity(new City(dtoIntLocation1.getCity().getId()));
        }
        if (dtoIntLocation1.getNeighborhood() != null) {
            location.setNeighborhood(new Neighborhood(dtoIntLocation1.getNeighborhood().getId()));
        }
        if (dtoIntLocation1.getSector() != null) {
            location.setSector(new Sector(dtoIntLocation1.getSector().getId()));
        }
        if (dtoIntLocation1.getState() != null) {
            location.setState(new State(dtoIntLocation1.getState().getId()));
        }
        if (dtoIntLocation1.getZone() != null) {
            location.setZone(new Zone(dtoIntLocation1.getZone().getId()));
        }
        return location;
    }

    public static DTOIntLocation dtoIntLocationMap(Location location) {
        DTOIntLocation dtoIntLocation = new DTOIntLocation();
        if (location.getCity() != null) {
            dtoIntLocation.setCity(new DTOIntCity(location.getCity().getId()));
        }
        if (location.getNeighborhood() != null) {
            dtoIntLocation.setNeighborhood(new DTOIntNeighborhood(location.getNeighborhood().getId()));
        }
        if (location.getSector() != null) {
            dtoIntLocation.setSector(new DTOIntSector(location.getSector().getId()));
        }
        if (location.getState() != null) {
            dtoIntLocation.setState(new DTOIntState(location.getState().getId()));
        }
        if (location.getZone() != null) {
            dtoIntLocation.setZone(new DTOIntZone(location.getZone().getId()));
        }
        return dtoIntLocation;
    }

    public static Image imageMap(DTOIntImage dtoIntImage) {
        Image image = new Image();
        BeanUtils.copyProperties(dtoIntImage, image);
        return image;
    }

    public static DTOIntImage dtoIntImageMap(Image image) {
        DTOIntImage dtoIntImage = new DTOIntImage();
        BeanUtils.copyProperties(image, dtoIntImage);
        return dtoIntImage;
    }

    public static Feature featureMap(DTOIntFeature dtoIntFeature) {
        Feature feature = new Feature();
        BeanUtils.copyProperties(dtoIntFeature, feature);
        return feature;
    }

    public static DTOIntFeature dtoIntFeatureMap(Feature feature) {
        DTOIntFeature dtoIntFeature = new DTOIntFeature();
        BeanUtils.copyProperties(feature, dtoIntFeature);
        return dtoIntFeature;
    }

}
