package com.bbva.czic.realestateprojects.facade.v00;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;
import com.wordnik.swagger.jaxrs.PATCH;

import com.bbva.jee.arq.spring.core.servicing.utils.BusinessServicesToolKit;

import com.bbva.czic.realestateprojects.facade.v00.dto.RealEstateProjects;
import com.bbva.czic.realestateprojects.business.ISrvIntRealEstateProjects;
import com.bbva.czic.realestateprojects.business.dto.DTOIntRealEstateProjects;
import com.bbva.czic.realestateprojects.facade.v00.mapper.Mapper;
import java.util.ArrayList;
import java.util.List;

@Path("/V00")
@SN(registryID = "SNCO1700009", logicalID = "realEstateProjects")
@VN(vnn = "V00")
@Api(value = "/realEstateProjects/V00", description = "Real estate projects contains all the basic information related to a project wich may include prices, areas, city locations, features etc.")
@Produces({MediaType.APPLICATION_JSON})
@Service
public class SrvRealEstateProjectsV00 implements ISrvRealEstateProjectsV00, com.bbva.jee.arq.spring.core.servicing.utils.ContextAware {

    private static I18nLog log = I18nLogFactory.getLogI18n(SrvRealEstateProjectsV00.class, "META-INF/spring/i18n/log/mensajesLog");

    public HttpHeaders httpHeaders;

    @Autowired
    BusinessServicesToolKit bussinesToolKit;

    @Autowired
    ISrvIntRealEstateProjects realEstateProjects;

    public UriInfo uriInfo;

    @Override
    public void setUriInfo(UriInfo ui) {
        this.uriInfo = ui;
    }

    @Override
    public void setHttpHeaders(HttpHeaders httpHeaders) {
        this.httpHeaders = httpHeaders;
    }

    @Autowired
    ISrvIntRealEstateProjects srvIntRealEstateProjects;

    @ApiOperation(value = "Obtiene un listado de los proyectos de Bienes Ra�ces", notes = "", response = Response.class)
    @ApiResponses(value = {
        @ApiResponse(code = -1, message = "mandatoryParametersMissing"),
        @ApiResponse(code = -1, message = "wrongParameters"),
        @ApiResponse(code = 200, message = "Found Sucessfully", response = Response.class),
        @ApiResponse(code = 500, message = "Technical Error")})
    @GET
    @SMC(registryID = "SMCCO1720036", logicalID = "listRealEstateProjects")
    @Override
    public List<RealEstateProjects> listRealEstateProjects(
            @ApiParam(value = "filter param") @DefaultValue("null") @QueryParam("$filter") String filter,
            @ApiParam(value = "fields param") @DefaultValue("null") @QueryParam("$fields") String fields,
            @ApiParam(value = "expands param") @DefaultValue("null") @QueryParam("$expands") String expands,
            @ApiParam(value = "order by param") @DefaultValue("null") @QueryParam("$sort") String sort) {

        String indicador = filter != null && !filter.isEmpty() ? filter : "C";

        List<DTOIntRealEstateProjects> intRealEstateProjectses = realEstateProjects.listRealEstateProjects(indicador);
        List<RealEstateProjects> realEstateProjectses = new ArrayList<RealEstateProjects>();
        for (DTOIntRealEstateProjects intRealEstateProjectse : intRealEstateProjectses) {
            realEstateProjectses.add(Mapper.realEstateProjectsMap(intRealEstateProjectse));
        }
        return realEstateProjectses;
    }

    @ApiOperation(value = "Obtiene el detalle de un proyecto de Bien Raiz", notes = "", response = RealEstateProjects.class)
    @ApiResponses(value = {
        @ApiResponse(code = -1, message = "mandatoryParametersMissing"),
        @ApiResponse(code = -1, message = "wrongParameters"),
        @ApiResponse(code = 200, message = "Found Sucessfully", response = Response.class),
        @ApiResponse(code = 500, message = "Technical Error")})
    @GET
    @Path("/{id}")
    @SMC(registryID = "SMCCO1720038", logicalID = "getRealEstateProject")
    @Override
    public RealEstateProjects getRealEstateProject(@ApiParam(value = "identifier param") @PathParam("id") String idRealEstateProjects) {
        DTOIntRealEstateProjects intRealEstateProjects = realEstateProjects.getRealEstateProject(idRealEstateProjects);
        RealEstateProjects estateProjects = Mapper.realEstateProjectsMap(intRealEstateProjects);
        return estateProjects;
    }

    @ApiOperation(value = "Crea un proyecto de Bien Raiz", notes = "", response = RealEstateProjects.class)
    @ApiResponses(value = {
        @ApiResponse(code = -1, message = "mandatoryParametersMissing"),
        @ApiResponse(code = -1, message = "wrongParameters"),
        @ApiResponse(code = 200, message = "Added Sucessfully", response = Response.class),
        @ApiResponse(code = 500, message = "Technical Error")})
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @SMC(registryID = "SMCCO1720037", logicalID = "createRealEstateProject")
    @Override
    public Response createRealEstateProject(@ApiParam(value = "Claim to add") RealEstateProjects infoRealEstateProjects) {

        DTOIntRealEstateProjects intRealEstateProjects = Mapper.dtoIntRealEstateProjectsMap(infoRealEstateProjects);

        realEstateProjects.createRealEstateProject(intRealEstateProjects);

        return Response.ok().build();
    }

    @ApiOperation(value = "Modifica los datos de un proyecto de Bien Raiz espec�fico", notes = "", response = Response.class)
    @ApiResponses(value = {
        @ApiResponse(code = -1, message = "mandatoryParametersMissing"),
        @ApiResponse(code = -1, message = "wrongParameters"),
        @ApiResponse(code = 200, message = "Modified Sucessfully", response = Response.class),
        @ApiResponse(code = 500, message = "Technical Error")})
    @PATCH
    @Path("/{idRealEstateProjects}")
    @Consumes({MediaType.APPLICATION_JSON})
    @SMC(registryID = "SMCCO1720039", logicalID = "modifyRealEstateProject")
    @Override
    public Response modifyRealEstateProject(@ApiParam(value = "Claim identifier") @PathParam("idRealEstateProjects") String idRealEstateProjects, @ApiParam(value = "Claim information") RealEstateProjects infoRealEstateProjects) {

        DTOIntRealEstateProjects intRealEstateProjects = Mapper.dtoIntRealEstateProjectsMap(infoRealEstateProjects);

        realEstateProjects.modifyRealEstateProject(idRealEstateProjects, intRealEstateProjects);

        return Response.ok().build();
    }

}
