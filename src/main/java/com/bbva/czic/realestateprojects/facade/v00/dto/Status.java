
package com.bbva.czic.realestateprojects.facade.v00.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "status", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlType(name = "status", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Status
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Unique Identifier Status", required = true)
    private String id;
    @ApiModelProperty(value = "Name Status", required = true)
    private String name;

    public Status() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
