package com.bbva.czic.realestateprojects.facade.v00.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "sector", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlType(name = "sector", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Sector
        implements Serializable {

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Unique Identifier", required = true)
    private String id;

    public Sector() {
        //default constructor
    }

    public Sector(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
