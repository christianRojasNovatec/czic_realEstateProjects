
package com.bbva.czic.realestateprojects.facade.v00.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "image", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlType(name = "image", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Image
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "URL", required = true)
    private String id;
    @ApiModelProperty(value = "Identifier associated to the state.", required = true)
    private String url;
    @ApiModelProperty(value = "Image description.", required = true)
    private String description;
    @ApiModelProperty(value = "Active Real Estate Projects.", required = true)
    private Boolean isActive;
    @ApiModelProperty(value = "Preferencial Real Estate Projects.", required = true)
    private Boolean isPreferencial;
    @ApiModelProperty(value = "Order image Real Estate Projects.", required = true)
    private String order;

    public Image() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsPreferencial() {
        return isPreferencial;
    }

    public void setIsPreferencial(Boolean isPreferencial) {
        this.isPreferencial = isPreferencial;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

}
