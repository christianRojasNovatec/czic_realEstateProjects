package com.bbva.czic.realestateprojects.facade.v00.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "neighborhood", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlType(name = "neighborhood", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Neighborhood
        implements Serializable {

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Unique Identifier", required = true)
    private String id;

    public Neighborhood() {
        //default constructor
    }

    public Neighborhood(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
