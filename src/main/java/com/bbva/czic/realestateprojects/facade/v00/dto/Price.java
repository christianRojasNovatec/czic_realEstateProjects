package com.bbva.czic.realestateprojects.facade.v00.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "price", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlType(name = "price", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Price
        implements Serializable {

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Amount.", required = true)
    private String amount;
    @ApiModelProperty(value = "Currency.", required = true)
    private String currency;

    public Price() {
        //default constructor
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
