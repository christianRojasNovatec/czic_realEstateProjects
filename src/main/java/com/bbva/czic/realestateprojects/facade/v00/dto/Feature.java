
package com.bbva.czic.realestateprojects.facade.v00.dto;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


import com.wordnik.swagger.annotations.ApiModelProperty;

@XmlRootElement(name = "feature", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlType(name = "feature", namespace = "urn:com:bbva:czic:realestateprojects:facade:v00:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Feature
    implements Serializable
{

    public final static long serialVersionUID = 1L;
    @ApiModelProperty(value = "Unique Identifier features", required = true)
    private String id;
    @ApiModelProperty(value = "Type features", required = true)
    private String featuresType;
    @ApiModelProperty(value = "Name features.", required = true)
    private String name;
    @ApiModelProperty(value = "Amount features.", required = true)
    private String amount;
    @ApiModelProperty(value = "features Description.", required = true)
    private String featuresDescription;

    public Feature() {
        //default constructor
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFeaturesType() {
        return featuresType;
    }

    public void setFeaturesType(String featuresType) {
        this.featuresType = featuresType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getFeaturesDescription() {
        return featuresDescription;
    }

    public void setFeaturesDescription(String featuresDescription) {
        this.featuresDescription = featuresDescription;
    }

}
