package com.bbva.czic.realestateprojects.dao.model.ugao;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>UGAO</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionUgao</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionUgao</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: COBD.CN.TMP.QGDTCCT.UGAO
 * UGAOMODIFICACION PROYECTO              OZ        UG1CUGAOBVDUGPO UGAOFE01            UGAO  NN3000NNNNNN    SSTN A      SNNSSNNN  NN                2017-02-22CICSDC112017-04-2400.41.43CE31417 2017-02-22-15.54.27.253281CICSDC110001-01-010001-01-01
 * FICHERO: COBD.CN.TMP.QGDTFDF.UGAOFE01
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�01�00001�CODPROY�CODIGO PROYECTO DEST�N�006�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�02�00007�VLRDESD�VALOR MINIMO DEL PRO�N�013�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�03�00020�CODCIUD�CODIGO CIUDAD PROYEC�N�004�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�04�00024�CODZONA�CODIGO ZONA        C�N�003�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�05�00027�SECTOR �NOMBRE SECTOR       �A�030�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�06�00057�BARRIO �NOMBRE BARRIO       �A�030�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�07�00087�ESTRATO�ESTRATO DEL PROYECTO�N�001�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�08�00088�INDESTA�ESTADO DEL PROYECTO �A�001�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�09�00089�VLRADMI�VALOR DE ADMINISTRAC�N�008�0�O�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�10�00097�AREAD  �AREA DESDE          �N�003�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�11�00100�NUMINMB�NUMERO DE INMUEBLES �N�003�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�12�00103�AREAPRI�AREA PRIVADA        �N�003�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�13�00106�AREACON�AREA CONSTRUIDA     �N�003�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�14�00109�NHABITA�NUMERO HABITACIONES �N�001�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�15�00110�NBANIOS�NUMERO BA�OS        �N�001�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�16�00111�CARACT1�DESCRIPCION INMUEBLE�A�100�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�17�00211�CARACT2�DESCRIPCION INMUEBLE�A�143�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�18�00354�VLRINMB�VALOR VENTA INMUEBLE�N�012�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�19�00366�INDACAB�INDICADOR ACABADOS  �A�001�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�20�00367�CODIMAG�CODIGO DE LA IMAGEN �N�006�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�21�00373�NOMIMAG�NOMBRE IMAGEN       �A�030�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�22�00403�URLIMAG�URL DE LA IMAGEN    �A�030�0�R�        �
 * UGAOFE01�MODIFICACION PROYECTO         �F�23�00433�23�00433�ORDENIM�ORDEN DE LA IMAGEN  �N�001�0�R�        �
</pre></code>
 * 
 * @see RespuestaTransaccionUgao
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "UGAO",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionUgao.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoUGAOFE01.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionUgao implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}