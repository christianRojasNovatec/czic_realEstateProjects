package com.bbva.czic.realestateprojects.dao.model.ugaj;


import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>UGAJCE01</code> de la transacci&oacute;n <code>UGAJ</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "UGAJCE01")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoUGAJCE01 {

	/**
	 * <p>Campo <code>INDLIS</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "INDLIS", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indlis;
	
}