package com.bbva.czic.realestateprojects.dao.model.ugal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>UGAL</code>
 * 
 * @see PeticionTransaccionUgal
 * @see RespuestaTransaccionUgal
 */
@Component
public class TransaccionUgal implements InvocadorTransaccion<PeticionTransaccionUgal,RespuestaTransaccionUgal> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionUgal invocar(PeticionTransaccionUgal transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionUgal.class, RespuestaTransaccionUgal.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionUgal invocarCache(PeticionTransaccionUgal transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionUgal.class, RespuestaTransaccionUgal.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}