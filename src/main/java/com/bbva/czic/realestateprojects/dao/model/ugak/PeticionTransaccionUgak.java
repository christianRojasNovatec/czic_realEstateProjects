package com.bbva.czic.realestateprojects.dao.model.ugak;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>UGAK</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionUgak</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionUgak</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: COBD.CN.TMP.QGDTCCT.UGAK
 * UGAKCREA UN PROYECTO DESTACADO         OZ        UG1CUGAKBVDUGPO UGAKFE01            UGAK  NN3000NNNNNN    SSTN A      SNNSSNNN  NN                2017-02-22CICSDC112017-04-2323.42.49CE31417 2017-02-22-15.54.27.253281CICSDC110001-01-010001-01-01
 * 
 * FICHERO: COBD.CN.TMP.QGDTFDF.UGAKFE01
 * UGAKFE01�E- ALTA DE UN PROYECTO        �F�11�00102�01�00001�CODPROY�CODIGO PROYECTO DEST�N�006�0�O�        �
 * UGAKFE01�E- ALTA DE UN PROYECTO        �F�11�00102�02�00007�VLRDESD�VALOR MINIMO DEL PRO�N�013�0�R�        �
 * UGAKFE01�E- ALTA DE UN PROYECTO        �F�11�00102�03�00020�CODCIUD�CODIGO CIUDAD PROYEC�N�004�0�R�        �
 * UGAKFE01�E- ALTA DE UN PROYECTO        �F�11�00102�04�00024�CODZONA�CODIGO ZONA        C�N�003�0�R�        �
 * UGAKFE01�E- ALTA DE UN PROYECTO        �F�11�00102�05�00027�SECTOR �NOMBRE SECTOR       �A�030�0�R�        �
 * UGAKFE01�E- ALTA DE UN PROYECTO        �F�11�00102�06�00057�BARRIO �NOMBRE BARRIO       �A�030�0�R�        �
 * UGAKFE01�E- ALTA DE UN PROYECTO        �F�11�00102�07�00087�ESTRATO�ESTRATO DEL PROYECTO�N�001�0�R�        �
 * UGAKFE01�E- ALTA DE UN PROYECTO        �F�11�00102�08�00088�INDESTA�ESTADO DEL PROYECTO �A�001�0�R�        �
 * UGAKFE01�E- ALTA DE UN PROYECTO        �F�11�00102�09�00089�VLRADMI�VALOR DE ADMINISTRAC�N�008�0�O�        �
 * UGAKFE01�E- ALTA DE UN PROYECTO        �F�11�00102�10�00097�AREAD  �AREA DESDE          �N�003�0�R�        �
 * UGAKFE01�E- ALTA DE UN PROYECTO        �F�11�00102�11�00100�NUMINMB�NUMERO DE INMUEBLES �N�003�0�R�        �
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionUgak
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "UGAK",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionUgak.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoUGAKFE01.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionUgak implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}