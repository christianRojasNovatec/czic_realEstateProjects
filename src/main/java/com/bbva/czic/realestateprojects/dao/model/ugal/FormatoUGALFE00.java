package com.bbva.czic.realestateprojects.dao.model.ugal;


import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>UGALFE00</code> de la transacci&oacute;n <code>UGAL</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "UGALFE00")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoUGALFE00 {

	/**
	 * <p>Campo <code>CODPROY</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "CODPROY", tipo = TipoCampo.ENTERO, longitudMinima = 6, longitudMaxima = 6)
	private Integer codproy;
	
}