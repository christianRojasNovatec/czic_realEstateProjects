package com.bbva.czic.realestateprojects.dao;


import com.bbva.czic.realestateprojects.dao.model.ugaj.FormatoUGAJCE01;
import com.bbva.czic.realestateprojects.dao.model.ugaj.PeticionTransaccionUgaj;
import com.bbva.czic.realestateprojects.dao.model.ugaj.RespuestaTransaccionUgaj;
import com.bbva.czic.realestateprojects.dao.model.ugaj.TransaccionUgaj;
import com.bbva.czic.realestateprojects.dao.model.ugak.FormatoUGAKFE01;
import com.bbva.czic.realestateprojects.dao.model.ugak.PeticionTransaccionUgak;
import com.bbva.czic.realestateprojects.dao.model.ugak.RespuestaTransaccionUgak;
import com.bbva.czic.realestateprojects.dao.model.ugak.TransaccionUgak;
import com.bbva.czic.realestateprojects.dao.model.ugal.FormatoUGALFE00;
import com.bbva.czic.realestateprojects.dao.model.ugal.FormatoUGALFS01;
import com.bbva.czic.realestateprojects.dao.model.ugal.PeticionTransaccionUgal;
import com.bbva.czic.realestateprojects.dao.model.ugal.RespuestaTransaccionUgal;
import com.bbva.czic.realestateprojects.dao.model.ugal.TransaccionUgal;
import com.bbva.czic.realestateprojects.dao.model.ugao.FormatoUGAOFE01;
import com.bbva.czic.realestateprojects.dao.model.ugao.PeticionTransaccionUgao;
import com.bbva.czic.realestateprojects.dao.model.ugao.RespuestaTransaccionUgao;
import com.bbva.czic.realestateprojects.dao.model.ugao.TransaccionUgao;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.ErrorMappingHelper;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = "realEstateProjects-dao")
public class RealEstateProjectsDAOImpl implements RealEstateProjectsDAO {

    @Autowired
    private TransaccionUgak ugak;

    @Autowired
    private TransaccionUgaj ugaj;

    @Autowired
    private TransaccionUgal ugal;

    @Autowired
    private TransaccionUgao ugao;

    @Autowired
    private ErrorMappingHelper emh;

    @Override
    public void createRealEstateProject(FormatoUGAKFE01 formatoUGAKFE01) {
        PeticionTransaccionUgak peticion = new PeticionTransaccionUgak();
        peticion.getCuerpo().getPartes().add(formatoUGAKFE01);
        RespuestaTransaccionUgak respuesta = ugak.invocar(peticion);
        BusinessServiceException bse = emh.toBusinessServiceException(respuesta);
        if (bse != null) {
            throw bse;
        }
    }

    @Override
    public List<CopySalida> listRealEstateProjects(FormatoUGAJCE01 formatoUGAJCE01) {
        PeticionTransaccionUgaj peticion = new PeticionTransaccionUgaj();
        peticion.getCuerpo().getPartes().add(formatoUGAJCE01);
        RespuestaTransaccionUgaj respuesta = ugaj.invocar(peticion);
        BusinessServiceException bse = emh.toBusinessServiceException(respuesta);
        if (bse != null) {
            throw bse;
        }
        return respuesta.getCuerpo().getPartes(CopySalida.class);
    }

    @Override
    public FormatoUGALFS01 getRealEstateProject(FormatoUGALFE00 formatoUGALFE01) {
        PeticionTransaccionUgal peticion = new PeticionTransaccionUgal();
        peticion.getCuerpo().getPartes().add(formatoUGALFE01);
        RespuestaTransaccionUgal respuesta = ugal.invocar(peticion);
        BusinessServiceException bse = emh.toBusinessServiceException(respuesta);
        if (bse != null) {
            throw bse;
        }
        return respuesta.getCuerpo().getParte(FormatoUGALFS01.class);
    }

    @Override
    public void modifyRealEstateProject(FormatoUGAOFE01 formatoUGAOFE01) {
        PeticionTransaccionUgao peticion = new PeticionTransaccionUgao();
        peticion.getCuerpo().getPartes().add(formatoUGAOFE01);
        RespuestaTransaccionUgao respuesta = ugao.invocar(peticion);
        BusinessServiceException bse = emh.toBusinessServiceException(respuesta);
        if (bse != null) {
            throw bse;
        }
    }

}
