package com.bbva.czic.realestateprojects.dao;

import com.bbva.czic.realestateprojects.dao.model.ugaj.FormatoUGAJCE01;
import com.bbva.czic.realestateprojects.dao.model.ugak.FormatoUGAKFE01;
import com.bbva.czic.realestateprojects.dao.model.ugal.FormatoUGALFE00;
import com.bbva.czic.realestateprojects.dao.model.ugal.FormatoUGALFS01;
import com.bbva.czic.realestateprojects.dao.model.ugao.FormatoUGAOFE01;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import java.util.List;

public interface RealEstateProjectsDAO {

    void createRealEstateProject(FormatoUGAKFE01 formatoUGAKFE01);
    
    void modifyRealEstateProject(FormatoUGAOFE01 formatoUGAOFE01);

    List<CopySalida> listRealEstateProjects(FormatoUGAJCE01 formatoUGAJCE01);

    FormatoUGALFS01 getRealEstateProject(FormatoUGALFE00 formatoUGALFE01);

}
