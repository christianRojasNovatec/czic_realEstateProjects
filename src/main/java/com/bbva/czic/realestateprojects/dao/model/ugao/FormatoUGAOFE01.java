package com.bbva.czic.realestateprojects.dao.model.ugao;


import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>UGAOFE01</code> de la transacci&oacute;n <code>UGAO</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "UGAOFE01")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoUGAOFE01 {

	/**
	 * <p>Campo <code>CODPROY</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "CODPROY", tipo = TipoCampo.ENTERO, longitudMinima = 6, longitudMaxima = 6)
	private Integer codproy;
	
	/**
	 * <p>Campo <code>VLRDESD</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 2, nombre = "VLRDESD", tipo = TipoCampo.ENTERO, longitudMinima = 13, longitudMaxima = 13)
	private Long vlrdesd;
	
	/**
	 * <p>Campo <code>CODCIUD</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 3, nombre = "CODCIUD", tipo = TipoCampo.ENTERO, longitudMinima = 4, longitudMaxima = 4)
	private Integer codciud;
	
	/**
	 * <p>Campo <code>CODZONA</code>, &iacute;ndice: <code>4</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 4, nombre = "CODZONA", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer codzona;
	
	/**
	 * <p>Campo <code>SECTOR</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "SECTOR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String sector;
	
	/**
	 * <p>Campo <code>BARRIO</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "BARRIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String barrio;
	
	/**
	 * <p>Campo <code>ESTRATO</code>, &iacute;ndice: <code>7</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 7, nombre = "ESTRATO", tipo = TipoCampo.ENTERO, longitudMinima = 1, longitudMaxima = 1)
	private Integer estrato;
	
	/**
	 * <p>Campo <code>INDESTA</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "INDESTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indesta;
	
	/**
	 * <p>Campo <code>VLRADMI</code>, &iacute;ndice: <code>9</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 9, nombre = "VLRADMI", tipo = TipoCampo.ENTERO, longitudMinima = 8, longitudMaxima = 8)
	private Integer vlradmi;
	
	/**
	 * <p>Campo <code>AREAD</code>, &iacute;ndice: <code>10</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 10, nombre = "AREAD", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer aread;
	
	/**
	 * <p>Campo <code>NUMINMB</code>, &iacute;ndice: <code>11</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 11, nombre = "NUMINMB", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer numinmb;
	
	/**
	 * <p>Campo <code>AREAPRI</code>, &iacute;ndice: <code>12</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 12, nombre = "AREAPRI", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer areapri;
	
	/**
	 * <p>Campo <code>AREACON</code>, &iacute;ndice: <code>13</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 13, nombre = "AREACON", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer areacon;
	
	/**
	 * <p>Campo <code>NHABITA</code>, &iacute;ndice: <code>14</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 14, nombre = "NHABITA", tipo = TipoCampo.ENTERO, longitudMinima = 1, longitudMaxima = 1)
	private Integer nhabita;
	
	/**
	 * <p>Campo <code>NBANIOS</code>, &iacute;ndice: <code>15</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 15, nombre = "NBANIOS", tipo = TipoCampo.ENTERO, longitudMinima = 1, longitudMaxima = 1)
	private Integer nbanios;
	
	/**
	 * <p>Campo <code>CARACT1</code>, &iacute;ndice: <code>16</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 16, nombre = "CARACT1", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 100, longitudMaxima = 100)
	private String caract1;
	
	/**
	 * <p>Campo <code>CARACT2</code>, &iacute;ndice: <code>17</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 17, nombre = "CARACT2", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 143, longitudMaxima = 143)
	private String caract2;
	
	/**
	 * <p>Campo <code>VLRINMB</code>, &iacute;ndice: <code>18</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 18, nombre = "VLRINMB", tipo = TipoCampo.ENTERO, longitudMinima = 12, longitudMaxima = 12)
	private Long vlrinmb;
	
	/**
	 * <p>Campo <code>INDACAB</code>, &iacute;ndice: <code>19</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 19, nombre = "INDACAB", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indacab;
	
	/**
	 * <p>Campo <code>CODIMAG</code>, &iacute;ndice: <code>20</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 20, nombre = "CODIMAG", tipo = TipoCampo.ENTERO, longitudMinima = 6, longitudMaxima = 6)
	private Integer codimag;
	
	/**
	 * <p>Campo <code>NOMIMAG</code>, &iacute;ndice: <code>21</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 21, nombre = "NOMIMAG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String nomimag;
	
	/**
	 * <p>Campo <code>URLIMAG</code>, &iacute;ndice: <code>22</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 22, nombre = "URLIMAG", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String urlimag;
	
	/**
	 * <p>Campo <code>ORDENIM</code>, &iacute;ndice: <code>23</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 23, nombre = "ORDENIM", tipo = TipoCampo.ENTERO, longitudMinima = 1, longitudMaxima = 1)
	private Integer ordenim;
	
}