package com.bbva.czic.realestateprojects.dao.model.ugal;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>UGAL</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionUgal</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionUgal</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: COBD.CN.TMP.QGDTCCT.UGAL
 * UGALDETALLE DE PROYECTOS DESTACADOS    UG        UG1CUGALBVDUGPO UGALFE00            UGY9  NN3000NNNNNN    SSTN A      SNNNSNNN  NN                2017-04-26CICSDC112017-05-2620.40.26CE31417 2017-04-26-19.33.11.982304CICSDC110001-01-010001-01-01
 * 
 * FICHERO: COBD.CN.TMP.QGDTFDF.UGALFE00
 * UGALFE00�E-DETALLE DE UN PROYECTO      �F�01�00006�01�00001�CODPROY�CODIGO DEL PROYECTO �N�006�0�R�        �
 * 
 * FICHERO: COBD.CN.TMP.QGDTFDF.UGALFS01
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�01�00016�CODPROY�CODIGO PROYECTO DEST�N�006�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�02�00025�VLRDESD�VALOR MINIMO DEL PRO�N�013�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�03�00041�CODCIUD�CODIGO CIUDAD PROYEC�N�004�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�04�00048�CODZONA�CODIGO ZONA        C�N�003�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�05�00054�SECTOR �NOMBRE SECTOR       �A�030�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�06�00087�BARRIO �NOMBRE BARRIO       �A�030�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�07�00120�ESTRATO�ESTRATO DEL PROYECTO�N�001�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�08�00124�INDESTA�ESTADO DEL PROYECTO �A�001�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�09�00128�VLRADMI�VALOR DE ADMINISTRAC�N�008�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�10�00139�AREAD  �AREA DESDE          �N�003�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�11�00145�NUMINMB�NUMERO DE INMUEBLES �N�003�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�12�00151�TIPINMU�TIPO DE INMUEBLE    �A�005�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�13�00159�AREAPRI�AREA PRIVADA        �N�003�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�14�00165�AREACON�AREA CONSTRUIDA     �N�003�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�15�00171�NHABITA�NUMERO HABITACIONES �N�001�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�16�00175�NBANIOS�NUMERO BA�OS        �N�001�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�17�00179�CARACT1�DESCRIPCION INMUEBLE�A�100�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�18�00282�CARACT2�DESCRIPCION INMUEBLE�A�143�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�19�00428�VLRINMB�VALOR VENTA INMUEBLE�N�012�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�20�00443�INDACAB�INDICADOR ACABADOS  �A�001�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�21�00447�CODIMAG�CODIGO DE LA IMAGEN �N�006�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�22�00456�NOMIMAG�NOMBRE IMAGEN       �A�030�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�23�00489�URLIMAG�URL DE LA IMAGEN    �A�030�0�S�        �
 * UGALFS01�DETALLE DE UN PROYECTO        �X�24�00438�24�00522�ORDENIM�ORDEN DE LA IMAGEN  �N�001�0�S�        �
 * 
 * FICHERO: COBD.CN.TMP.QGDTFDX.UGAL
 * UGALUGALFS01COPY    UG1CUGAL1S                             CICSDC112017-04-26-19.55.26.356830CICSDC112017-04-26-19.55.26.356841
 * 
</pre></code>
 * 
 * @see RespuestaTransaccionUgal
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "UGAL",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionUgal.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoUGALFE00.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionUgal implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}