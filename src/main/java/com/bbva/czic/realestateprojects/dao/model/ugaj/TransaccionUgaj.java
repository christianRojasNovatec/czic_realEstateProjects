package com.bbva.czic.realestateprojects.dao.model.ugaj;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>UGAJ</code>
 * 
 * @see PeticionTransaccionUgaj
 * @see RespuestaTransaccionUgaj
 */
@Component
public class TransaccionUgaj implements InvocadorTransaccion<PeticionTransaccionUgaj,RespuestaTransaccionUgaj> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionUgaj invocar(PeticionTransaccionUgaj transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionUgaj.class, RespuestaTransaccionUgaj.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionUgaj invocarCache(PeticionTransaccionUgaj transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionUgaj.class, RespuestaTransaccionUgaj.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}