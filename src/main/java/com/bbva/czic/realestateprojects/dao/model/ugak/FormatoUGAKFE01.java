package com.bbva.czic.realestateprojects.dao.model.ugak;


import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

/**
 * Formato de datos <code>UGAKFE01</code> de la transacci&oacute;n <code>UGAK</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "UGAKFE01")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoUGAKFE01 {

	/**
	 * <p>Campo <code>CODPROY</code>, &iacute;ndice: <code>1</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 1, nombre = "CODPROY", tipo = TipoCampo.ENTERO, longitudMinima = 6, longitudMaxima = 6)
	private Integer codproy;
	
	/**
	 * <p>Campo <code>VLRDESD</code>, &iacute;ndice: <code>2</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 2, nombre = "VLRDESD", tipo = TipoCampo.ENTERO, longitudMinima = 13, longitudMaxima = 13)
	private Long vlrdesd;
	
	/**
	 * <p>Campo <code>CODCIUD</code>, &iacute;ndice: <code>3</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 3, nombre = "CODCIUD", tipo = TipoCampo.ENTERO, longitudMinima = 4, longitudMaxima = 4)
	private Integer codciud;
	
	/**
	 * <p>Campo <code>CODZONA</code>, &iacute;ndice: <code>4</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 4, nombre = "CODZONA", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer codzona;
	
	/**
	 * <p>Campo <code>SECTOR</code>, &iacute;ndice: <code>5</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 5, nombre = "SECTOR", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String sector;
	
	/**
	 * <p>Campo <code>BARRIO</code>, &iacute;ndice: <code>6</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 6, nombre = "BARRIO", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 30, longitudMaxima = 30)
	private String barrio;
	
	/**
	 * <p>Campo <code>ESTRATO</code>, &iacute;ndice: <code>7</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 7, nombre = "ESTRATO", tipo = TipoCampo.ENTERO, longitudMinima = 1, longitudMaxima = 1)
	private Integer estrato;
	
	/**
	 * <p>Campo <code>INDESTA</code>, &iacute;ndice: <code>8</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 8, nombre = "INDESTA", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 1, longitudMaxima = 1)
	private String indesta;
	
	/**
	 * <p>Campo <code>VLRADMI</code>, &iacute;ndice: <code>9</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 9, nombre = "VLRADMI", tipo = TipoCampo.ENTERO, longitudMinima = 8, longitudMaxima = 8)
	private Integer vlradmi;
	
	/**
	 * <p>Campo <code>AREAD</code>, &iacute;ndice: <code>10</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 10, nombre = "AREAD", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer aread;
	
	/**
	 * <p>Campo <code>NUMINMB</code>, &iacute;ndice: <code>11</code>, tipo: <code>ENTERO</code>
	 */
	@Campo(indice = 11, nombre = "NUMINMB", tipo = TipoCampo.ENTERO, longitudMinima = 3, longitudMaxima = 3)
	private Integer numinmb;
	
}