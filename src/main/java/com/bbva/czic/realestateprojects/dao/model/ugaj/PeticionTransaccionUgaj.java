package com.bbva.czic.realestateprojects.dao.model.ugaj;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>UGAJ</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionUgaj</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionUgaj</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: COBD.CN.TMP.QGDTCCT.UGAJ
 * UGAJLISTADO PROY VIVIENDA ONLINE       OZ        UG1CUGAJBVDUGPO UGAJCE01            UGAJ  NN3000NNNNNN    SSTN A      SNNSSNNN  NN                2017-02-22CICSDC112017-04-2322.14.44CE31417 2017-02-22-15.54.27.253281CICSDC110001-01-010001-01-01
 * FICHERO: COBD.CN.TMP.QGDTFDF.UGAJCE01
 * UGAJCE01�E - LISTADO DE PROYECTOS      �F�01�00001�01�00001�INDLIS �INDICADOR LISTADO   �A�001�0�R�        �
 * FICHERO: COBD.CN.TMP.QGDTFDF.UGAJFS01
 * UGAJFS01�S - LISTADO DE PROYECTOS      �X�06�00098�01�00001�CODPRO �CODIGO DEL PROYECTO �N�006�0�S�        �
 * UGAJFS01�S - LISTADO DE PROYECTOS      �X�06�00098�02�00007�NOMPROR�NOMBRE PROYECTO     �A�050�0�S�        �
 * UGAJFS01�S - LISTADO DE PROYECTOS      �X�06�00098�03�00057�CIUDAD �CIUDAD DEL PROYECTO �A�020�0�S�        �
 * UGAJFS01�S - LISTADO DE PROYECTOS      �X�06�00098�04�00077�ESTADO �ESTADO DEL PROYECTO �A�020�0�S�        �
 * UGAJFS01�S - LISTADO DE PROYECTOS      �X�06�00098�05�00097�SIMAGEN�IND IMAGENES        �A�001�0�S�        �
 * UGAJFS01�S - LISTADO DE PROYECTOS      �X�06�00098�06�00098�SINMUEB�IND INMUEBLES       �A�001�0�S�        �
 * 
 * FICHERO: COBD.CN.TMP.QGDTFDX.UGAJ
 * UGAJUGAJFS01COPY    UG1CUGAJ1S                             CE31417 2017-04-23-22.47.49.767307CE31417 2017-04-23-22.47.49.767317
</pre></code>
 * 
 * @see RespuestaTransaccionUgaj
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "UGAJ",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionUgaj.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoUGAJCE01.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionUgaj implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}