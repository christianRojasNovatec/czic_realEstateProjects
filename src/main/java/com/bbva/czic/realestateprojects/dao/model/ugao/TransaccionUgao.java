package com.bbva.czic.realestateprojects.dao.model.ugao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>UGAO</code>
 * 
 * @see PeticionTransaccionUgao
 * @see RespuestaTransaccionUgao
 */
@Component
public class TransaccionUgao implements InvocadorTransaccion<PeticionTransaccionUgao,RespuestaTransaccionUgao> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionUgao invocar(PeticionTransaccionUgao transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionUgao.class, RespuestaTransaccionUgao.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionUgao invocarCache(PeticionTransaccionUgao transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionUgao.class, RespuestaTransaccionUgao.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}