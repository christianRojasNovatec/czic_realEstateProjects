package com.bbva.czic.realestateprojects.dao.model.ugak;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>UGAK</code>
 * 
 * @see PeticionTransaccionUgak
 * @see RespuestaTransaccionUgak
 */
@Component
public class TransaccionUgak implements InvocadorTransaccion<PeticionTransaccionUgak,RespuestaTransaccionUgak> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionUgak invocar(PeticionTransaccionUgak transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionUgak.class, RespuestaTransaccionUgak.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionUgak invocarCache(PeticionTransaccionUgak transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionUgak.class, RespuestaTransaccionUgak.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {}	
}