package com.bbva.czic.realestateprojects.business;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.bbva.jee.arq.spring.core.servicing.test.BusinessServiceTestContextLoader;
import com.bbva.jee.arq.spring.core.servicing.test.MockInvocationContextTestExecutionListener;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = BusinessServiceTestContextLoader.class, locations = {"classpath*:/META-INF/spring/applicationContext-*.xml", "classpath:/META-INF/spring/business-service.xml", "classpath:/META-INF/spring/business-service-test.xml"})
@TestExecutionListeners(listeners = {MockInvocationContextTestExecutionListener.class, DependencyInjectionTestExecutionListener.class})
public class SrvIntRealEstateProjectsTest {
	
	@Autowired
	ISrvIntRealEstateProjects srv;

		
	@Test
	public void testGetRealEstateProjectss() {
		//TODO: call srv.getRealEstateProjectss
	}

		
	@Test
	public void testGetRealEstateProjects() {
		//TODO: call srv.getRealEstateProjects
	}

		
	@Test
	public void testAddRealEstateProjects() {
		//TODO: call srv.addRealEstateProjects
	}

		
	@Test
	public void testPartialModifyRealEstateProjects() {
		//TODO: call srv.partialModifyRealEstateProjects
	}

	
	
}

